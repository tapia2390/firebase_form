<?php
namespace Proceso;

class Process 
{
    private $file = __DIR__.'/config.json';

    public function __construct(){
        $this->data = json_decode(file_get_contents($this->file));
    }

    //Envía la notificación a todos los usuarios de la APP Android/iOS
    public function sendAll($X){
        $msg = array(
                'title'        => $this->data->title,
                'body'         => $this->data->body,
                'icon'         => $this->data->icon,
                'sound'        => $this->data->sound,
                'click_action' => $X['actividad'],
                'color'        => $this->data->color
            );
        
        $data = array(
            "message"  => $X['mensaje'],//Mensaje a enviar desde el formulario
            "asunto"   => $X['asunto'],//Asunto del Mensaje desde el formulario
        );

        $fields = array(
                    'to'=> '/topics/'.$this->data->topic, 
                    'notification'	=> $msg,
                    'data'=>$data
                );
        $headers = array(
                    'Authorization: key=' . $this->data->apikey, 
                    'Content-Type: application/json'
                );


        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        return (object) array('code'=>'200','data'=>$result);
    }//



    //Envía la notificación a un usuario especifico por su token
    public function sendUser($X){
        $msg = array(
                'title'        => $this->data->title,
                'body'         => $this->data->body,
                'icon'         => $this->data->icon,
                'sound'        => $this->data->sound,
                'click_action' => $X['actividad'],
                'color'        => $this->data->color
            );
        
        $data = array(
            "message"  => $X['mensaje'],//Mensaje a enviar desde el formulario
            "asunto"   => $X['asunto'],//Asunto del Mensaje desde el formulario
        );

        $fields = array(
                    'to'=> $X['token'], //Aqui se recibe el Token del usuario registrado
                    'notification'	=> $msg,
                    'data'=>$data
                );
        $headers = array(
                    'Authorization: key=' . $this->data->apikey, 
                    'Content-Type: application/json'
                );


        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        return (object) array('code'=>'200','data'=>$result);
    }//



}

?>